FROM openjdk:11
COPY ./target/spring-microservices-k8s-0.0.1.jar spring-microservice-k8s.jar
ENTRYPOINT ["java", "jar", "/spring-microservice-k8s.jar"]
EXPOSE 8084