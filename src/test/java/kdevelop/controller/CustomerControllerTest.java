package kdevelop.controller;

import kdevelop.model.Customer;
import kdevelop.repository.CustomerRepositoty;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

@SpringBootTest
class CustomerControllerTest {

    @Autowired
    private CustomerController customerController;
    @MockBean
    private CustomerRepositoty customerRepositoty;

    private Customer customer;

    @BeforeEach
    void setUp() {
        customer =  new Customer(1L,"Peter", "Parker", "peter@gmail.com", "0687452196");
    }

    @AfterEach
    void tearDown() {
        Mockito.reset(customerRepositoty);
    }

    @Test
    public void testGetCustomerById(){
        when(customerRepositoty.findCustomerByPersonID(1L)).thenReturn(customer);
        Assertions.assertEquals(customer, customerController.getCustomerById(1L));
    }

    @Test
    public void testGetCustomers(){
        List<Customer> customers = Arrays.asList(
                new Customer("Peter", "Parker", "peter@gmail.com", "0687452196"),
                new Customer("Bruce", "Wayne", "bruce@gmail.com", "0687455487"),
                new Customer("Clark", "Kent", "kentpeter@gmail.com", "0687445874")
        );
        when(customerRepositoty.findAll()).thenReturn(customers);
        Assertions.assertEquals(customers, customerController.getAllCustomers());
    }
}