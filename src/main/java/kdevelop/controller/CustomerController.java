package kdevelop.controller;

import kdevelop.model.Customer;
import kdevelop.repository.CustomerRepositoty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/person")
public class CustomerController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerRepositoty customerRepositoty;

    @GetMapping
    List<Customer> getAllCustomers(){
        LOGGER.info(String.format("Getting all persons"));
        return this.customerRepositoty.findAll();
    }

    @GetMapping("/{id}")
    Customer getCustomerById(@PathVariable("id") Long id){
        LOGGER.info(String.format("Getting person %s", id));
        return this.customerRepositoty.findCustomerByPersonID(id);
    }
}
