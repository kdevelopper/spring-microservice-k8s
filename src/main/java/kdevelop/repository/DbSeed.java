package kdevelop.repository;

import kdevelop.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

@Component
public class DbSeed implements CommandLineRunner {

    private static final Logger LOGGER = LoggerFactory.getLogger(DbSeed.class);


    @Autowired
    private CustomerRepositoty customerRepositoty;

    @Override
    public void run(String... args) throws Exception {
        LOGGER.info("Deleting database...");
        this.customerRepositoty.deleteAll();

        List<Customer> customers = Arrays.asList(
                new Customer("Peter", "Parker", "peter@gmail.com", "0687452196"),
                new Customer("Bruce", "Wayne", "bruce@gmail.com", "0687455487"),
                new Customer("Clark", "Kent", "kentpeter@gmail.com", "0687445874")
        );
        LOGGER.info("Saving customers...");
        this.customerRepositoty.saveAll(customers);
        LOGGER.info("Fetching customers...");
        this.customerRepositoty.findAll().forEach(customer -> System.out.println(customer));
    }
}
