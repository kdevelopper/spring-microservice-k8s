package kdevelop.repository;

import kdevelop.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepositoty extends JpaRepository<Customer, Long> {

    Customer findCustomerByPersonID(Long id);
}
