package kdevelop.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "Persons")
public class Customer implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long customerID;
    @Column(nullable = false)
    private String first_name;
    @Column(nullable = false)
    private String last_name;
    private String email;
    @Column(length = 10)
    private String phone_number;

    public Customer() {
    }

    public Customer(Long customerID, String first_name, String last_name, String email, String phone_number) {
        this.customerID = customerID;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
    }

    public Customer(String first_name, String last_name, String email, String phone_number) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.phone_number = phone_number;
    }

    public Long getCustomerID() {
        return customerID;
    }

    public void setCustomerID(Long personID) {
        this.customerID = personID;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }
}
